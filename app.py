from flask import *


app = Flask(__name__)


@app.route("/")
def index():
	return render_template("index.html")


@app.route("/classroom")
def classroom():
    return render_template("classroom.html")
    

@app.route("/login")
def login():
	return "login"


@app.route("/logout")
def logout():
	return "logout"


@app.route("/addaccount")
def add_account():
	return "add account"


@app.route("/removeaccount")
def remove_account():
	return "remove account"


@app.route("/uploadfile")
def upload_file():
	return "upload file"


@app.route("/getfiles")
def get_files():
	return "get files"


if __name__ == "__main__":
	app.run(debug=True)

