# Classroom

A web application written in python/flask, to mimic some features of a classroom.

![screenshot](screenshots/app.png)

## Set-up

Requires Flask
After cloning or downloading, run app.py file.
```
python3 app.py
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
